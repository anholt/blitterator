/*
 * Copyright © 2020 Google, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


#ifndef __CONFIG_H__
#define __CONFIG_H__

#include <stdint.h>

#define GPU_ID 618

/**
 * To simplify things, keep these powers-of-two (so that we can ignore pitch
 * vs width)
 *
 * 2k * 2k * 4byte/pixel => 16MB of read and write bandwidth per blit.
 */
#define BLIT_WIDTH      2048llu
#define BLIT_HEIGHT     2048llu

/**
 * Use a 4-byte per pixel format
 */
#define BLIT_CPP        4
#define BLIT_FMT        RB6_R8G8B8A8_UNORM
#define BLIT_IFMT       R2D_UNORM8

#define BLIT_TILE_MODE  TILE6_LINEAR

/**
 * NUM_BLITS * 2 buffers are allocated, so that neither src nor dest ends up
 * in the gpu's cache.
 */
#define NUM_BLITS       16

/**
 * Number of iterations of NUM_BLITS
 */
#define NUM_ITERATIONS  16


/*
 * random stuff that needs to go somewhere:
 */

#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))
#define VOID2U64(x) ((uint64_t)(unsigned long)(x))

static inline unsigned
fui(float f)
{
	union {
		float f;
		uint32_t ui;
	} fi;
	fi.f = f;
	return fi.ui;
}

#if !defined(__LP64__)
/* 32-bit needs mmap64 for 64-bit offsets */
#  define os_mmap(addr, length, prot, flags, fd, offset) \
             mmap64(addr, length, prot, flags, fd, offset)
#else
/* assume large file support exists */
#  define os_mmap(addr, length, prot, flags, fd, offset) \
             mmap(addr, length, prot, flags, fd, offset)
#endif

#endif /* __CONFIG_H__ */
