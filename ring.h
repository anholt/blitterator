/*
 * Copyright © 2020 Google, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


#ifndef __RING_H__
#define __RING_H__

#include <assert.h>
#include <stdint.h>

#include "kmd.h"
#include "adreno_common.xml.h"
#include "adreno_pm4.xml.h"


struct fd_ringbuffer {
	struct kmd_device *dev;
	struct kmd_bo *bo;
	uint32_t *cur, *end, *start;
};

static inline struct fd_ringbuffer *
fd_ringbuffer_new(struct kmd_device *dev, uint32_t sizedwords)
{
	struct fd_ringbuffer *ring = calloc(1, sizeof(*ring));

	ring->dev   = dev;
	ring->bo    = kmd_bo_alloc(dev, 4 * sizedwords);
	ring->cur   = kmd_bo_map(ring->bo);
	ring->start = ring->cur;
	ring->end   = ring->start + sizedwords;

	return ring;
}

static inline void
fd_ringbuffer_flush(struct fd_ringbuffer *ring)
{
	uint32_t fence =
		kmd_dev_submit(ring->dev, ring->bo, ring->cur - ring->start);
	kmd_dev_waitfence(ring->dev, fence);
}

static inline void
OUT_RING(struct fd_ringbuffer *ring, uint32_t data)
{
	(*ring->cur++) = data;
}

static inline void
OUT_RELOC(struct fd_ringbuffer *ring, struct kmd_bo *bo)
{
	uint64_t iova = kmd_bo_iova(bo);

	OUT_RING(ring, iova);
	OUT_RING(ring, iova >> 32);
}

static inline void
BEGIN_RING(struct fd_ringbuffer *ring, uint32_t ndwords)
{
	assert(ring->cur + ndwords < ring->end);
}

static inline unsigned
_odd_parity_bit(unsigned val)
{
	/* See: http://graphics.stanford.edu/~seander/bithacks.html#ParityParallel
	 * note that we want odd parity so 0x6996 is inverted.
	 */
	val ^= val >> 16;
	val ^= val >> 8;
	val ^= val >> 4;
	val &= 0xf;
	return (~0x6996 >> val) & 1;
}

static inline void
OUT_PKT4(struct fd_ringbuffer *ring, uint16_t regindx, uint16_t cnt)
{
	BEGIN_RING(ring, cnt+1);
	OUT_RING(ring, CP_TYPE4_PKT | cnt |
			(_odd_parity_bit(cnt) << 7) |
			((regindx & 0x3ffff) << 8) |
			((_odd_parity_bit(regindx) << 27)));
}

static inline void
OUT_PKT7(struct fd_ringbuffer *ring, uint8_t opcode, uint16_t cnt)
{
	BEGIN_RING(ring, cnt+1);
	OUT_RING(ring, CP_TYPE7_PKT | cnt |
			(_odd_parity_bit(cnt) << 15) |
			((opcode & 0x7f) << 16) |
			((_odd_parity_bit(opcode) << 23)));
}

static inline void
OUT_WFI(struct fd_ringbuffer *ring)
{
	OUT_PKT7(ring, CP_WAIT_FOR_IDLE, 0);
}


#endif /* __RING_H__ */
