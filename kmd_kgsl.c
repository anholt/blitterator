/*
 * Copyright © 2020 Google, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <err.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <linux/time.h>
#include <fcntl.h>
#include <unistd.h>

#include "config.h"
#include "kmd.h"
#include "msm_kgsl.h"

/**
 * This is the implementation of the kernel-mode-driver for kgsl.
 */

struct kmd_device {
	int fd;
	uint32_t context_id;
};

struct kmd_bo {
	struct kmd_device *dev;
	uint64_t iova;
	uint32_t size;
	uint32_t id;
};

static int
call(struct kmd_device *dev, unsigned long request, void *arg)
{
	int ret;

	do {
		ret = ioctl(dev->fd, request, arg);
	} while (ret == -1 && (errno == EINTR || errno == EAGAIN));
	return ret;
}


/**
 * Open kernel device
 */
struct kmd_device *
kmd_dev_open(void)
{
	struct kmd_device *dev;
	int fd, ret;

	fd = open("/dev/kgsl-3d0", O_RDWR);
	if (fd < 0)
		return NULL;

	dev = calloc(1, sizeof(*dev));
	dev->fd = fd;

	struct kgsl_drawctxt_create req = {
			.flags = KGSL_CONTEXT_SAVE_GMEM |
				KGSL_CONTEXT_NO_GMEM_ALLOC |
				KGSL_CONTEXT_PREAMBLE,
	};

	ret = call(dev, IOCTL_KGSL_DRAWCTXT_CREATE, &req);
	if (ret) {
		err(ret, "could not create context");
		return NULL;
	}

	dev->context_id = req.drawctxt_id;

	return dev;
}

/**
 * Submit cmdstream to the kernel, returning a fence/timestamp value.
 */
uint32_t
kmd_dev_submit(struct kmd_device *dev, struct kmd_bo *cmds,
		uint32_t sizedwords)
{
	struct kgsl_command_object cmd = {
			.offset = 0,
			.gpuaddr = cmds->iova,
			.size = sizedwords,
			.flags = KGSL_CMDLIST_IB,
			.id = cmds->id,

	};
	struct kgsl_gpu_command req = {
			.flags = KGSL_CMDBATCH_SUBMIT_IB_LIST,
			.context_id = dev->context_id,
			.cmdlist = VOID2U64(&cmd),
			.numcmds = 1,
			.cmdsize = sizeof(struct kgsl_command_object),
	};
	int ret;

	ret = call(dev, IOCTL_KGSL_GPU_COMMAND, &req);
	if (ret) {
		err(ret, "submit failed");
	}

	return req.timestamp;
}

/**
 * Wait for fence.
 */
void
kmd_dev_waitfence(struct kmd_device *dev, uint32_t fence)
{
	struct kgsl_device_waittimestamp_ctxtid req = {
			.context_id = dev->context_id,
			.timestamp = fence,
			.timeout = INT32_MAX,
	};
	int ret;

	ret = call(dev, IOCTL_KGSL_DEVICE_WAITTIMESTAMP_CTXTID, &req);
	if (ret) {
		err(ret, "waittimestamp failed");
	}
}

/**
 * Allocate a GPU buffer object with specified size (in bytes).
 */
struct kmd_bo *
kmd_bo_alloc(struct kmd_device *dev, int size)
{
	struct kmd_bo *bo;
	struct kgsl_gpumem_alloc_id req = {
			.size = size,
	};
	int ret;

	ret = call(dev, IOCTL_KGSL_GPUMEM_ALLOC_ID, &req);
	if (ret) {
		err(ret, "allocation failed");
		return NULL;
	}

	bo = calloc(1, sizeof(*bo));
	bo->dev  = dev;
	bo->iova = req.gpuaddr;
	bo->size = req.mmapsize;
	bo->id   = req.id;

	return bo;
}

/**
 * Map the specified buffer for CPU access.  The buffer should be
 * mapped uncached/writecombine.
 */
void *
kmd_bo_map(struct kmd_bo *bo)
{
	uint64_t offset = bo->id << 12;
	void *ptr;

	ptr = mmap(0, bo->size, PROT_READ | PROT_WRITE, MAP_SHARED,
			bo->dev->fd, offset);
	if (ptr == MAP_FAILED) {
		err(-1, "mmap failed");
	}

	return ptr;
}

/**
 * Get the buffers GPU iova
 */
uint64_t
kmd_bo_iova(struct kmd_bo *bo)
{
	return bo->iova;
}
